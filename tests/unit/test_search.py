import glob
from typing import Dict, List

import pandas as pd
from nltk.corpus import wordnet

from Engine import (Document, build_index, clean_text, get_wordnet_pos,
                    read_data, retrieve, score)


def test_clean_text():
    start_text_1 = "Hey buddy, I think you've got the wrong door,"
    start_text_2 = " the leather club's two blocks down"
    start_text = start_text_1 + start_text_2
    result_text = clean_text(start_text)
    assert (
        result_text
        == "hey buddy think get wrong door leather club's two block"
    )


def test_get_wordnet_pos():
    adj = "JJ"
    noun = "NN"
    verb = "VV"
    adverb = "RR"
    assert get_wordnet_pos(adj) == wordnet.ADJ
    assert get_wordnet_pos(noun) == wordnet.NOUN
    assert get_wordnet_pos(verb) == wordnet.VERB
    assert get_wordnet_pos(adverb) == wordnet.ADV


def test_read_data():
    files_list = glob.glob("**/tmdb_5000_movies.csv", recursive=True)
    data = read_data(files_list[0])
    assert isinstance(data, pd.DataFrame)


def test_build_index():
    files_list = glob.glob("**/tmdb_5000_movies.csv", recursive=True)
    data = read_data(files_list[0])
    index, raw_data = build_index(data)
    assert isinstance(index, Dict)
    assert isinstance(raw_data, List)


def test_score():
    query = "batman"
    files_list = glob.glob("**/tmdb_5000_movies.csv", recursive=True)
    data = read_data(files_list[0])
    doc = Document(
        str(data.loc[0, "original_title"]),
        str(data.loc[0, "overview"]),
    )
    score_test = score(query, doc)
    assert isinstance(score_test, float)


def test_retrieve():
    query = "batman"
    files_list = glob.glob("**/tmdb_5000_movies.csv", recursive=True)
    data = read_data(files_list[0])
    index, raw_data = build_index(data)
    doc_list = retrieve(index, raw_data, query)
    assert isinstance(doc_list, List)
